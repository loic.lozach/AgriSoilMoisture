#!/usr/bin/python3
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import subprocess,  os
import argparse, shutil

def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images



if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Batch calibration and orthorectification of Sentinel-1 images with ESA SNAP
        """)
    
    parser.add_argument('-s1dir', action='store', required=True, help='Directory containing Sentinel-1 zip file')
    parser.add_argument('-graph', action='store', required=True, help='ESA SNAP xml graph file')
    #parser.add_argument('-auxdir', action='store', required=True, help='Directory for ESA SNAP auxiliary files (DEM)')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory for Sentinel-1 calibrated files')
    parser.add_argument('--zipfile', dest='zipfile', action='store_true', help='Process .zip file or .safe directory (default zip)')
    parser.add_argument('--safefile', dest='zipfile', action='store_false')
    parser.set_defaults(zipfile=True)
#     parser.add_argument('--docker', dest='docker', action='store_true', help='Use Docker ESA Snap image for processing, default true')
#     parser.add_argument('--no-docker', dest='docker', action='store_false')
#     parser.set_defaults(docker=True)
    
    
    args=parser.parse_args()
    
    graphfile = args.graph #"/mnt/GEOSUD_WA/SoilMoisture/_PROD/lib/sentinel1_Cal_TC.xml"
    #basedata = args.auxdir #"/mnt/GEOSUD_WA/SoilMoisture/_PROD"
    optvoldata = "-v "+os.path.abspath(args.s1dir)+":"+os.path.abspath(args.s1dir)
    #optvoluser = "-v "+args.auxdir+":/home/lozach/.snap"
    my_env = os.environ.copy()
        
    s1dir=[]
    if args.zipfile:
        s1dir=search_files(args.s1dir, 'S1', 'zip', 'f')
        lenext=-3
    else:
        s1dir=search_files(args.s1dir, 'S1', 'safe', 'd')
        lenext=-4
    
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
    
#     shutil.copy(args.graph, args.outdir)
#     graphfile = os.path.join(args.outdir, os.path.basename(args.graph))
    print("using: " +graphfile)
#     if not os.path.exists(os.path.join(args.auxdir,".snap")):
#         auxcmd = "docker cp esa-snap:/home/lozach/.snap "+args.auxdir
#         p = subprocess.Popen(auxcmd, shell=True, env=my_env)
#         p.wait() 
        
    i=0
    for dataset in s1dir: 
        i+=1 
        
        filename = os.path.basename(dataset) 
        s1output = os.path.join(args.outdir, filename[:lenext]+"dim")
        cmd = "gpt "+graphfile+" -Pinput="+dataset+" -Poutput="+s1output
#         if args.docker:
#             cmd = "docker run "+optvoldata+" esa-snap "+cmd 
            #+" "+optvoluser
            
        print("command : "+cmd)
        p = subprocess.Popen(cmd, shell=True, env=my_env)
        p.wait() 
        
        print(str(i)+"/"+str(len(s1dir))+" images processed")
        print("################################################################")

        
    print("Done!")
        
