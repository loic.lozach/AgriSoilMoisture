#!/usr/bin/python
__author__ = "Loic Lozach"
__date__ = "$Dec 14, 2018 12:40:21 PM$"


import json, os, sys, argparse
try:
    from osgeo import ogr, osr, gdal
except:
    sys.exit('ERROR: cannot find GDAL/OGR modules')
import otbApplication


def create_dict_assets(smhref, qklhref):
    mvassets = {
            "soil_moisture":{
                "href":smhref,
                "type":"image/vnd.stac.geotiff"
            }, "thumbnail":{
                "href":qklhref,
                "type":"image/png"
            }
    }
    return mvassets

def create_dict_properties(datetime,platform,gsd,epsg,title="Sentinel-1 derived soil Moisture product at Plot scale (S2MP)"):
    #gsd => resolution en metre
    mvproperties = {
        "datetime":datetime,
        "title":title,
        "platform":platform,
        "eo:gsd":gsd,
        "eo:epsg":epsg
    }
    return mvproperties

def create_dict_geometry(coordinates,geomtype="Polygon"):
    mvgeom = {
        "type":geomtype,
        "coordinates":[coordinates]
    }
    return mvgeom


def create_dict_image(imgid, geometry, properties, assets, imgtype="Feature"):
    mvdata = {
        "id":imgid,
        "type":imgtype,
        "geometry":geometry,
        "properties":properties,
        "assets":assets
    }
    
    return mvdata

def search_files(directory='.', extension='tif', resolution='MV_'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    print resolution
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                
                #print(os.path.join(dirpath, name))
                abspath = os.path.abspath(os.path.join(dirpath, name))
                images.append(abspath)

    return images


def generate_quicklook(img, lut, quick):
    # The following line creates an instance of the Superimpose application
    app1 = otbApplication.Registry.CreateApplication("Superimpose")
    
    # The following lines set all the application parameters:
    app1.SetParameterString("in", img)
    app1.SetParameterString("out", "temp1.tif")
    app1.SetParameterString("op", "labeltocolor")
    app1.SetParameterString("method", "custom")
    app1.SetParameterString("method.custom.lut", lut)
    
    print("Launching... Resampling")
    # The following line execute the application
    app1.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app2 = otbApplication.Registry.CreateApplication("Quicklook")
    
    # The following lines set all the application parameters:
    app2.SetParameterInputImage("in", app1.GetParameterOutputImage("out"))
    app2.SetParameterString("out", quick)
    app2.SetParameterInt("sx", 1024)
    
    print("Launching... Resampling")
    # The following line execute the application
    app2.ExecuteAndWriteOutput() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
def reprojectRaster(src,match_ds,dst_filename):
	src_proj = src.GetProjection()
	src_geotrans = src.GetGeoTransform()

	match_proj = match_ds.GetProjection()
	match_geotrans = match_ds.GetGeoTransform()
	wide = match_ds.RasterXSize
	high = match_ds.RasterYSize

	dst = gdal.GetDriverByName('GTiff').Create(dst_filename, wide, high, 1, gdalconst.GDT_Int16)
	dst.SetGeoTransform( match_geotrans )
	dst.SetProjection( match_proj)

	gdal.ReprojectImage(src, dst, src_proj, match_proj, gdalconst.GRA_Bilinear)

	del dst # Flush
	return(gdal.Open(dst_filename,gdalconst.GA_ReadOnly))
    
    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Create geojson metadata recursively for Soil Moisture images collections 
        """)
    

    #####################################################
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    #####################################################    
    
    parser.add_argument('-mvdir', action='store', required=True, help='Directory containing Soil Moisture data')
    parser.add_argument('--quicklook', dest='quicklook', action='store_true', help='Generate quicklook images')
    parser.add_argument('--no-quicklook', dest='quicklook', action='store_false')
    parser.set_defaults(quicklook=True)
    parser.add_argument('-lut', action='store', required=False, help='LUT file for quicklook generation')
    parser.add_argument('--overwrite', dest='overwrite', action='store_true', help='Overwrite already existing files')
    parser.add_argument('--no-overwrite', dest='overwrite', action='store_false', help='Overwrite already existing files')
    parser.set_defaults(overwrite=False)
    
        
    args=parser.parse_args()
    if args.quicklook:
        if None == args.lut:
            print "LUT is required for quicklook generation"
            exit
    
    mvtifs=[]
    mvtifs=search_files(args.mvdir)
    if len(mvtifs) == 0 :
        print "No soil moisture image found"
        exit
        
    for file in mvtifs:
        absfile = os.path.abspath(file)
        spltpath = absfile.split("/")
        
        geojsonfile = absfile[:-3]+"JSON"
        quicklookfile = absfile[:-3]+"PNG"
        
        if os.path.exists(geojsonfile) and args.overwrite :
            print geojsonfile + " already exists! Skipping file."
            continue
        
        filename = os.path.basename(file)
        spltfilename = filename.split("_")
        
        date = spltfilename[len(spltfilename)-1][:-4]
        date_in_format = date[0:4]+"-"+date[4:6]+"-"+date[6:8]+date[8:11]+":"+date[11:13]+":"+date[13:15]+"Z"
        
        mv_plateform = spltfilename[1]
        
        ind = spltpath.index("SoilMoisture")
        mv_url = "https://thisme.cines.teledetection.fr/" + '/'.join(spltpath[ind+1:])
        qkl_url = mv_url[:-3]+"PNG"
        
        raster = gdal.Open(absfile)
        
        transform = raster.GetGeoTransform()
        xOrigin = transform[0]
        yOrigin = transform[3]
        pixelWidth = transform[1]
        pixelHeight = transform[5]
        
        proj = osr.SpatialReference(wkt=raster.GetProjection())
        mv_epsg = int(proj.GetAttrValue('AUTHORITY',1))
        
        rows = raster.RasterYSize
        cols = raster.RasterXSize
        
        lastX = xOrigin + cols *pixelWidth
        lastY = yOrigin + rows *pixelHeight
        xCenter = xOrigin + cols/2 *pixelWidth
        yCenter = yOrigin + rows/2 *pixelHeight
        
        mv_coordinates = [[xOrigin,lastY],[xOrigin,yOrigin],[lastX,yOrigin],[lastX,lastY],[xOrigin,lastY]]
        
        mv_properties = create_dict_properties(date_in_format,mv_plateform,pixelWidth,mv_epsg)
        
        mv_assets = create_dict_assets(mv_url, qkl_url)
        
        mv_geometry = create_dict_geometry(mv_coordinates)
        
        mv_geojson = create_dict_image(filename[:-3], mv_geometry, mv_properties, mv_assets)
        
        with open(geojsonfile,'w') as jsonf:
            json.dump(mv_geojson, jsonf, indent=4)
        
        if not args.quicklook :
            continue
        
        generate_quicklook(absfile, args.lut, quicklookfile)
            
