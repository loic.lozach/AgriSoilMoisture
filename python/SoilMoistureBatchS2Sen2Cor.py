#!/usr/bin/python3
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import subprocess, os
import argparse
#import otbApplication

def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images

#def ndvi_pipeline(nir, red, mask, out):
#    
#    app0 = otbApplication.Registry.CreateApplication("Superimpose")
#    app0.SetParameterString("inm",mask)
#    app0.SetParameterString("inr",nir)
#    app0.SetParameterString("out", "temp0.tif")
#    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
#    app0.SetParameterString("interpolator", "nn")
#    app0.SetParameterInt("ram",4000)
#    app0.Execute()
#    
#    app1 = otbApplication.Registry.CreateApplication("BandMathX")
#    # Define Input im2: Band Red (B4)
##    app1.AddParameterStringList("il",[nir,red])
#    app1.AddParameterStringList("il",nir)
#    app1.AddParameterStringList("il",red)
#    app1.SetParameterString("out", "temp1.tif")
#    app1.SetParameterString("exp", "(im1b1-im2b1)/(im1b1+im2b1)>0?(im1b1-im2b1)/(im1b1+im2b1)*100:0")
#    app1.SetParameterInt("ram",4000)
#    app1.Execute()
#    
#    app2 = otbApplication.Registry.CreateApplication("BandMath")
#    app2.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
#    # Define Input im2: Band Red (B4)
#    app2.AddImageToParameterInputImageList("il", app0.GetParameterOutputImage("out"))
#    app2.SetParameterString("out", "temp2.tif")
#    app2.SetParameterString("exp", "im2b1==0?im1b1:0")
#    app2.SetParameterInt("ram",4000)
#    app2.Execute()
#    
#    app3 = otbApplication.Registry.CreateApplication("ManageNoData")
#    app3.SetParameterInputImage("in", app2.GetParameterOutputImage("out"))
#    app3.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
#    app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
#    app3.SetParameterString("mode", "changevalue")
#    app3.SetParameterInt("ram",4000)
#    
#    app3.ExecuteAndWriteOutput()

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Compute cloud-masked NDVI from Sentinel2 L2A (Sen2Cor) dimap using current directory.
        """)
    
    parser.add_argument('-s2l1dir', action='store', required=True, help='Directory containing Sentinel2 L1C dimap')
#    parser.add_argument('-ndvidir', action='store', required=True, help='Output directory for computed NDVIs')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
    args=parser.parse_args()
    
    s2l1s=[]
    s2l1s=search_files(args.s2l1dir, 'S2', 'SAFE', 'd')
    
    
    
    for dataset in s2l1s: #b:,ns
        if not ( os.path.isdir(dataset) and dataset.startswith('S2') and dataset.find('L1C') ):
            continue
        
        my_env = os.environ.copy()
        my_env["PATH"] = "/work/esa-snap/Sen2Cor-02.05.05-Linux64/bin:" + my_env["PATH"]    
        cmd='source /work/esa-snap/Sen2Cor-02.05.05-Linux64/L2A_Bashrc && L2A_Process '+dataset
        print("command : "+cmd)
        p = subprocess.Popen(cmd, shell=True, env=my_env)
        p.wait() 
        
        print("Sen2Cor using file : "+name)
        
    print("Done!")
        
