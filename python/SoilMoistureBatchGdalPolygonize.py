#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Batch gdal_tranlate to extract an AOI define with -projwin

Auteur: Loïc Lozac'h
"""

import os, argparse
from subprocess import Popen, PIPE

def search_files(directory='.', matchnaming='MV',extension='TIF',fictype='f'):
    images=[]
    matchnaming = matchnaming.upper()
    extension = extension.upper()
    fictype = fictype.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype != 'd':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if  name.upper().find(matchnaming) == 0 and name.upper().endswith(extension):
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if dirname.upper().find(matchnaming) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    



if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Batch gdal_polygonize.py on Soil Moisture geotiff files and recursively on mvdir.
        It will create a "SHP" dir on each level where MV files are found
        """)
    
    parser.add_argument('-mvdir', action='store', required=True, help='Directory containing SoilMoisture geotiff to process')
    
    args=parser.parse_args()
    
    mvfiles=[]
    mvfiles=search_files(args.mvdir)
    
    fieldname = "MVx5"
    
    for file in mvfiles :
        outdir = os.path.join(os.path.dirname(file),"SHP")
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        destfile = os.path.join(outdir,os.path.basename(file)[:-3]+"shp")
        layer = os.path.basename(file)[:-4]
        cmd=["gdal_polygonize.py", file, "-f", "ESRI Shapefile", destfile, layer, fieldname]
        process_command(cmd)
        
            
        
        
            
    
    




