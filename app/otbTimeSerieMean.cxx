#include "otbWrapperApplication.h"
#include "otbWrapperCompositeApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
//Stack
#include "otbTensorflowSource.h"
// Stats
#include "otbSentinel2Functors.h"
#include "itkUnaryFunctorImageFilter.h"


namespace otb
{
namespace Wrapper
{
class TimeSerieMean : public CompositeApplication
{
public:
  typedef TimeSerieMean             Self;
  typedef CompositeApplication                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(TimeSerieMean, otb::Wrapper::CompositeApplication);
  

  /** Typedefs for image concatenation */
  typedef otb::TensorflowSource<FloatVectorImageType>                       TFSourceType;
  
  /** Statistics */
  typedef otb::Functor::TimeSerieMeanFunctor<FloatVectorImageType::PixelType,FloatImageType::PixelType> TSMeanFilterType;
  typedef itk::UnaryFunctorImageFilter<FloatVectorImageType,FloatImageType, TSMeanFilterType> TSMeanFunctorFilterType;

private:
  void DoInit()
  {
    SetName("TimeSerieMean");
    SetDescription("Create average map over time series images");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loïc Lozac'h");
    SetDocSeeAlso(" ");
    
    //Application
    ClearApplications();
    
    AddApplication("ConcatenateImages", "stack", "Create stack");

    // Input images
    ShareParameter("il","stack.il");
    
//    // Input images
//    AddParameter(ParameterType_InputImageList,    "il",    "Input serie to average");
//
//    // Output image
    this->AddParameter(ParameterType_OutputImage,   "out", "Output image");
    SetParameterDescription                  ("out", "Mean image over the time serie blabla test");
    

  }

  void DoUpdateParameters()
  {
      
  }

 

  /*
   * Do the work
   */
  void DoExecute()
  {
//    FloatVectorImageListType::Pointer list = GetParameterImageList("il");
//
//    // Create a stack of input images
//    
//    m_ImageSource.Set(list);
      
    ExecuteInternal("stack");
    
    
    // Produce the map
    m_TSMeanFunctorFilter = TSMeanFunctorFilterType::New();
    m_TSMeanFunctorFilter->SetInput(
                static_cast<FloatVectorImageType*>(GetInternalApplication("stack")->GetParameterOutputImage("out")));
    
    

    SetParameterOutputImage("out", m_TSMeanFunctorFilter->GetOutput());

  }


  // Stack
  TFSourceType             m_ImageSource;
  
  // Filter
//  FloatVectorImageListType::Pointer m_ImageList;
  TSMeanFunctorFilterType::Pointer m_TSMeanFunctorFilter;
};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::TimeSerieMean)
