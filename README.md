# Agricultural Areas Soil Moisture applications

This module contains applications for Operational Soil Moisture Mapping over Agricultural Areas with Sentinel-1 and Sentinel-2

## User manual

* The [Soil Moisture Pipeline for agricultural areas](https://gitlab.irstea.fr/loic.lozach/AgriSoilMoisture/blob/master/SoilMoisture-ManuelUtilisateur.pdf) is available.


# Docker install

* To avoid tedious install of TensorFlow and OTB from source, a dockerfile is available [here](https://gitlab.irstea.fr/loic.lozach/AgriSoilMoisture/tree/master/tools/docker). 

* It uses the latest image of Rémi Cresson's [mld4eo/otbtf1.6](https://hub.docker.com/r/mdl4eo/otbtf1.6) on dockerhub. 

A conflict can happened between your machine and the image which defines an otbuser with uid 1001. This dockerfile fix this issue by modifying otbuser to uid 1000, so if you have a particular user configuration please modify the file accordingly. The dockerfile also add the group vboxsf at gid 998 for a docker install on VirtualBox with shared folders.


# How to install from source
This remote module has been tested successfully on Ubuntu 18.04.

## Dependencies

* The [OTB](https://www.orfeo-toolbox.org/) library
* The [TensorFlow](https://gitlab.irstea.fr/remi.cresson/otbtf) remote module for OTB

## Build OTB
First, **build the latest *develop* branch of OTB from sources**. You can check the [OTB documentation](https://www.orfeo-toolbox.org/SoftwareGuide/SoftwareGuidech2.html) which details all the steps, if fact it is quite easy thank to the SuperBuild.

## Build TensorFlow
Then you have to **build Tensorflow from source** except if you want to use only the sampling applications of OTBTensorflow (in this case, skip this section).
Follow [the instructions](https://www.tensorflow.org/install/install_sources) to build Tensorflow.

## Build this remote module
Finally, we can build this module.
Clone the repository in your the OTB sources directory for remote modules (something like `otb/Modules/Remote/`).
Re configure OTB with cmake of ccmake, and set the following variables

 - **Module_OTBTensorflow** to **ON**
 - **OTB_USE_TENSORFLOW** to **ON** (if you set to OFF, you will have only the sampling applications)
 - **TENSORFLOW_CC_LIB** to `/path/to/lib/libtensorflow_cc.so`
 - **TENSORFLOW_FRAMEWORK_LIB** to `/path/to/lib/libtensorflow_framework.so`
 - **tensorflow_include_dir** to `/path/to/include`

Re build and install OTB.
Done !

## InvertSARModel application

### Description

This application computes soil moisture with an invertion model based on Sentinel-1 Sigma0-VV, Sentinel-1 Incidence Angle and Sentinel-2 NDVI.
The implemented pipeline is described in [El Hajj et al](https://dx.doi.org/10.3390/rs9121292).

### How to use it

InvertSARModel is an OTBApplication.

```
This is the  (InvertSARModel) application, version 6.7.0

Invert the IEM Model over SAR data
Parameters: 
MISSING -insarvv     <string>         Input SAR VV image  (mandatory)
MISSING -insarth     <string>         Input SAR Theta image  (mandatory)
MISSING -inndvi      <string>         Input NDVI  image  (mandatory)
MISSING -inlabels    <string>         Input label image  (mandatory)
        -nodatalabel <int32>          Input label image no-data value  (mandatory, default value is 0)
        -model       <group>          Tensorflow model parameters 
MISSING -model.dir   <string>         TensorFlow SavedModel directory  (mandatory)
MISSING -out         <string> [pixel] Output image  [pixel=uint8/uint16/int16/uint32/int32/float/double/cint16/cint32/cfloat/cdouble] (default value is float) (mandatory)
        -inxml       <string>         Load otb application from xml file  (optional, off by default)
        -progress    <boolean>        Report progress 
        -help        <string list>    Display long help (empty list), or help for given parameters keys


Use -help param1 [... paramN] to see detailed documentation of those parameters.

Examples: 
None


```

InvertSARModel can be used as any OTB application (gui, command line, python, c++, ...).
To add the application in QGIS, just copy the file "_InvertSARModel.xml_" into the qgis/python/plugins/processing/algs/otb/description/5.0.0/ path.

## SoilMoistureMaskCreator application

### Description


### How to use it

SoilMoistureSegmentationFiltering is an OTBApplication.

```

This is the  (SoilMoistureSegmentationFiltering) application, version 6.7.0

Filter NDVI segmented image for agricultural areas
Parameters: 
MISSING -labels     <string>         Image reference (Segmented NDVI)  (mandatory)
MISSING -lulc       <string>         LandUseLandCover (default Theia OSO's landuse)  (mandatory)
        -agrivalues <string list>    Agricultural Landuse values (default Theia OSO = 11 12)  (mandatory, default value is 11 12)
MISSING -out        <string> [pixel] Output image  [pixel=uint8/uint16/int16/uint32/int32/float/double/cint16/cint32/cfloat/cdouble] (default value is uint32) (mandatory)
        -ram        <int32>          Available RAM (Mb)  (optional, off by default, default value is 128)
        -inxml      <string>         Load otb application from xml file  (optional, off by default)
        -progress   <boolean>        Report progress 
        -help       <string list>    Display long help (empty list), or help for given parameters keys

Use -help param1 [... paramN] to see detailed documentation of those parameters.

Examples: 
None

```

SoilMoistureSegmentationFiltering can be used as any OTB application (gui, command line, python, c++, ...).
To add the application in QGIS, just copy the file "_SoilMoistureSegmentationFiltering.xml_" into the qgis/python/plugins/processing/algs/otb/description/5.0.0/ path.

## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact
Loïc Lozac'h (IRSTEA)

