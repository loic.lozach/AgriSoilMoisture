/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   otbStreamingStatisticsMapFromLabelImageFilter2.hxx
 * Author: otbuser
 *
 * Created on January 14, 2021, 12:54 AM
 */

#ifndef OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_HXX
#define OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_HXX
#include "otbStreamingStatisticsMapFromLabelImageFilter2.h"

namespace otb
{
    
template <class TInputVectorImage, class TLabelImage>
typename PersistentStreamingStatisticsMapFromLabelImageFilter2<TInputVectorImage, TLabelImage>::VectorValidPixelsRatioMapType
PersistentStreamingStatisticsMapFromLabelImageFilter2<TInputVectorImage, TLabelImage>::GetValidPixelsRatioMap() const
{
  return m_CountPixel;
}

template <class TInputVectorImage, class TLabelImage>
void PersistentStreamingStatisticsMapFromLabelImageFilter2<TInputVectorImage, TLabelImage>::Synthetize()
{
  // Update temporary accumulator
  AccumulatorMapType outputAcc;
  auto               endAcc = outputAcc.end();

  for (auto const& threadAccMap : m_AccumulatorMaps)
  {
    for (auto const& it : threadAccMap)
    {
      auto label = it.first;
      auto itAcc = outputAcc.find(label);
      if (itAcc == endAcc)
      {
        outputAcc.emplace(label, it.second);
      }
      else
      {
        itAcc->second.Update(it.second);
      }
    }
  }

  // Publish output maps
  for (auto& it : outputAcc)
  {
    const LabelPixelType label     = it.first;
    const auto&          bandCount = it.second.GetBandCount();
    const auto&          sum       = it.second.GetSum();
    const auto&          sqSum     = it.second.GetSqSum();

    // Count
    m_LabelPopulation[label] = it.second.GetCount();

    // Mean & stdev
    RealVectorPixelType mean(sum);
    RealVectorPixelType std(sqSum);
    RealVectorValidPixelsRatioType vectorcount;
    vectorcount.SetSize(mean.GetSize());
    for (unsigned int band = 0; band < mean.GetSize(); band++)
    {
      // Number of valid pixels in band
      auto count = bandCount[band];
      // Ratio of valid pixels
      vectorcount[band] = count/it.second.GetCount();
      // Mean
      mean[band] /= count;

      // Unbiased standard deviation (not sure unbiased is useful here)
      const double variance = (sqSum[band] - (sum[band] * mean[band])) / (count - 1);
      std[band]             = std::sqrt(variance);
      
    }
    m_CountPixel[label] = vectorcount;
    
    m_MeanRadiometricValue[label]  = mean;
    m_StDevRadiometricValue[label] = std;

    // Min & max
    m_MinRadiometricValue[label] = it.second.GetMin();
    m_MaxRadiometricValue[label] = it.second.GetMax();
  }
}
    
}


#endif /* OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_HXX */

